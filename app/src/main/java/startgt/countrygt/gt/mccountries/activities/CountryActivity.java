package startgt.countrygt.gt.mccountries.activities;

import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.View;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;

import objects.beans.Country;
import objects.ws.ServerResponse;
import objects.ws.response.GetCountryResponse;
import ws.CountryClient;
import startgt.countrygt.gt.mccountries.R;
import startgt.countrygt.gt.mccountries.helpers.CustomDialogs;
import startgt.countrygt.gt.mccountries.helpers.interfaces.DialogInterface;
import startgt.countrygt.gt.mccountries.helpers.interfaces.ObjectTreatment;
import objects.adapters.CountryAdapterRecycler;


public class CountryActivity extends AppCompatActivity implements DialogInterface {
    private GetCountryResponse getCountryResponse;
    private ProgressDialog progressDialog;
    private RecyclerView rv_list_countries;
    private List countryList = new ArrayList<Country>();
    CountryAdapterRecycler adapter;


    public void initComponents(){
        rv_list_countries = (RecyclerView)findViewById(R.id.rv_country);
        countryList = Country.findAll(this);
        Log.i("paises", "size "+countryList.size());
        if(countryList!=null) {
            GridLayoutManager gridLayoutManager = new GridLayoutManager(this,1);
            rv_list_countries.setLayoutManager(gridLayoutManager);
            adapter = new CountryAdapterRecycler(countryList, this, R.layout.country_recycler_layout);
            rv_list_countries.setAdapter(adapter);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.title_activity_country_list);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog = ProgressDialog.show(CountryActivity.this,"","Procesando datos...");
                CountryClient countryClient = new CountryClient(CountryActivity.this,CountryActivity.this);
                countryClient.execute();
            }
        });
        initComponents();

    }





    @Override
    public void processAction(ServerResponse serverResponse) {
        progressDialog.cancel();

        if(serverResponse.isSucessful()){
            getCountryResponse = ObjectTreatment.parseStrToObject(serverResponse.getJson(),GetCountryResponse.class);
            if(getCountryResponse.isSucessful()){
                Log.i("Cant." , " : "+ getCountryResponse.getCountryList().size());
                countryList = getCountryResponse.getCountryList();
                    for (Country country : getCountryResponse.getCountryList()) {
                        country.save(this);
                    }
                initComponents();
            }else{
                Log.i("EndPoint Response",getCountryResponse.getMessage());
                CustomDialogs.showOk(null, getCountryResponse.getMessage(), "Aceptar", this);
            }
        }else{
            CustomDialogs.showOk(null, serverResponse.getMessage(),"Aceptar",this);
        }
    }
    @Override
    public void ejectAction() {

    }
}
