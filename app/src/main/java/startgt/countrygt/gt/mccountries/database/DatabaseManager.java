package startgt.countrygt.gt.mccountries.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import objects.beans.Country;


/**
 * Created by Jonathan.Zepeda on 20/03/2018
 */

public class DatabaseManager extends SQLiteOpenHelper {

    private static String DATABASE_NAME = "Paises";

    private static int DATABASE_VERSION = 1;


    @Override
    public void onDowngrade(SQLiteDatabase sqLiteDatabase,
                            int oldVersion, int newVersion){
        Country.deleteTable(sqLiteDatabase);
    }

    public DatabaseManager(Context context){
        super(context, DATABASE_NAME,null, DATABASE_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.i("SQLManager", "Create Country");
        Country.createTable(sqLiteDatabase);
    }



}
