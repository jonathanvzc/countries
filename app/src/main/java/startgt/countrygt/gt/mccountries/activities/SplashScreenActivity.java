package startgt.countrygt.gt.mccountries.activities;

import startgt.countrygt.gt.mccountries.R;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;




public class SplashScreenActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 5_000;
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(runOption, SPLASH_TIME_OUT);
    }

    private Runnable runOption = new Runnable() {
        @Override
        public void run() {
            goToList();
        }
    };

    private void goToList(){
        Intent intent = new Intent(this, CountryActivity.class);
        startActivity(intent);
        finish();
    }
}
