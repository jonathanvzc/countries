package startgt.countrygt.gt.mccountries.helpers.interfaces;


import objects.ws.ServerResponse;

/**
 * Created by Jonathan.Zepeda on 20/03/2018
 */

public interface DialogInterface {
    void ejectAction();
    void processAction(ServerResponse serverResponse);
}
