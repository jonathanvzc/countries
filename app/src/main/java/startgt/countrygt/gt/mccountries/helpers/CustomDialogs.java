package startgt.countrygt.gt.mccountries.helpers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;

/**
 * Created by Jonathan.Zepeda on 20/03/2018
 */

public class CustomDialogs {
    public static void showOk(String title, String message, String btn, Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if(!TextUtils.isEmpty(title)){
            builder.setTitle(title);
        }
        if(!TextUtils.isEmpty((message))){
            builder.setMessage(message);
        }
       builder.setNeutralButton(btn, new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialogInterface, int i) {

           }
       });
        builder.create().show();
    }
}
