package ws;

import android.content.Context;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

import startgt.countrygt.gt.mccountries.helpers.interfaces.ObjectTreatment;
import objects.ws.ServerResponse;

import startgt.countrygt.gt.mccountries.R;

/**
 * Created by Jonathan.Zepeda on 20/03/2018
 */

public class ServerClient {
    public static ServerResponse connect(Context context, String endPoint, Object request){
        ServerResponse result = new ServerResponse();

        try {
            Log.d("Endpoint", endPoint);
            URL url = new URL(endPoint);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(30_000);
            connection.setReadTimeout(45_000);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type","application/json");
            connection.setRequestProperty("Accept","application/json");

            //Enviar Informacion
            if (request != null){
                BufferedOutputStream outputStream = new BufferedOutputStream(connection.getOutputStream());
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream,"utf-8"));
                String jsonRequest = ObjectTreatment.parseObjectToStr(request);
                Log.d("ServerConnection", "jsonRequest" + jsonRequest);
                writer.write(jsonRequest);
                writer.flush();
                writer.close();
                outputStream.close();
            }

            connection.connect();

            Log.d("Response Code", ": " + connection.getResponseCode());
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK){
                BufferedReader bufferedReader =
                        new BufferedReader(
                                new InputStreamReader(connection.getInputStream()
                                )
                        );
                String jsonResult="";
                String temp;
                while((temp = bufferedReader.readLine()) != null){
                    jsonResult += temp;
                }
                //validar que si sea un json
                result.setJson(jsonResult);
                //result.setJson(parceResultStream(connection.getInputStream());
                result.setSucessful(true);

            }else{
                result.setMessage(context.getString(R.string.msj_malformed));
            }
        }catch(MalformedURLException e){
            result.setMessage(context.getString(R.string.msj_malformed));
            e.printStackTrace();
        }catch(SocketTimeoutException e){
            result.setMessage(context.getString(R.string.msj_malformed));
            e.printStackTrace();
        }catch (IOException e){
            result.setMessage(context.getString(R.string.msj_malformed));
            e.printStackTrace();
        }

        return result;
    }
}
