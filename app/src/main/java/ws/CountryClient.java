package ws;

import android.content.Context;
import android.os.AsyncTask;

import startgt.countrygt.gt.mccountries.helpers.interfaces.DialogInterface;
import objects.ws.ServerResponse;

/**
 * Created by Jonathan.Zepeda on 20/03/2018
 */

public class CountryClient extends AsyncTask<Void,Void,ServerResponse>{
    private final String ENDPOINT ="https://www.mcd.com.gt/usmcdgt01/rest/gw/exec/?" +
            "noCia=40&" +
            "appId=356a192b7913b04c54574d18c28d46e6395428ab&" +
            "version=1&" +
            "jSonParam=%7B%22webservice%22%3A%22Paises%22%20,%22no_cia%22%3A%2240%22%7D&" +
            "hash=GMei55XcVODLTblEe1DrHLGZd95SfHzJ1QIbufgqaTw%3D";
    private Context context;
    private DialogInterface dialogInterface;

    public CountryClient(Context context, DialogInterface dialogInterface){
        this.context= context;
        this.dialogInterface = dialogInterface;
    }

    @Override
    protected ServerResponse doInBackground(Void... voids) {
        return ServerClient.connect(context, ENDPOINT, null);
    }

    @Override
    protected void onPostExecute(ServerResponse serverResponse) {
        dialogInterface.processAction(serverResponse);

    }
}
