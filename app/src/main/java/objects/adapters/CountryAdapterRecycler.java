package objects.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import objects.beans.Country;
import objects.holders.CountryHolder;

/**
 * Created by Jonathan.Zepeda on 20/03/2018.
 */

public class CountryAdapterRecycler extends RecyclerView.Adapter<CountryHolder>{
    private List<Country> countryList;
    private Activity activity;
    private int resourceLayout;


    public CountryAdapterRecycler(List<Country> countryList, Activity activity, int resourceLayout) {
        this.countryList = countryList;
        this.activity = activity;
        this.resourceLayout = resourceLayout;
    }

    @Override
    public CountryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CountryHolder(LayoutInflater.from(activity).inflate(resourceLayout,parent,false));
    }

    @Override
    public void onBindViewHolder(CountryHolder holder, int position) {
        final Country country = countryList.get(position);
        holder.getTv_pais().setText(country.getPAIS().toString());
        holder.getTv_nombre().setText(country.getNOMBRE().toString());
        holder.getTv_empresa().setText(country.getNO_CIA().toString());
        holder.getTv_idioma().setText(country.getIDIOMA().toString());
        Context context = holder.getIv_bandera().getContext();
        Picasso.with(context).load(country.getFLAG()).into(holder.getIv_bandera());
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return countryList.size();
    }

}
