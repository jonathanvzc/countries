package objects.holders;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import startgt.countrygt.gt.mccountries.R;

/**
 * Created by Jonathan.Zepeda on 20/03/2018
 */

public class CountryHolder extends RecyclerView.ViewHolder {
    private CardView cv_container;
    private TextView tv_nombre;
    private TextView tv_empresa;
    private ImageView iv_bandera;
    private TextView tv_pais;
    private TextView tv_idioma;

    public CountryHolder(View itemView) {
        super(itemView);
        this.tv_nombre = itemView.findViewById(R.id.tv_nombre);
        this.tv_empresa = itemView.findViewById(R.id.tv_empresa);
        this.tv_idioma = itemView.findViewById(R.id.tv_idioma);
        cv_container = itemView.findViewById(R.id.cv_container);
        this.iv_bandera = itemView.findViewById(R.id.iv_bandera);
        this.tv_pais = itemView.findViewById(R.id.tv_pais);

    }

    public CardView getCv_container() {
        return cv_container;
    }
    public TextView getTv_pais() {
        return tv_pais;
    }

    public void setTv_pais(TextView tv_pais) {
        this.tv_pais = tv_pais;
    }

    public TextView getTv_empresa() {
        return tv_empresa;
    }

    public void setTv_empresa(TextView tv_empresa) {
        this.tv_empresa = tv_empresa;
    }

    public TextView getTv_idioma() {
        return tv_idioma;
    }

    public void setCv_container(CardView cv_container) {
        this.cv_container = cv_container;
    }

    public ImageView getIv_bandera() {
        return iv_bandera;
    }

    public void setIv_bandera(ImageView iv_bandera) {
        this.iv_bandera = iv_bandera;
    }

    public TextView getTv_nombre() {
        return tv_nombre;
    }

    public void setTv_nombre(TextView tv_nombre) {
        this.tv_nombre = tv_nombre;
    }

    public void setTv_idioma(TextView tv_idioma) {
        this.tv_idioma = tv_idioma;
    }
}
