package objects.beans;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

import startgt.countrygt.gt.mccountries.database.DatabaseManager;
import startgt.countrygt.gt.mccountries.database.Model;

/**
 * Created by Jonathan.Zepeda on 20/03/2018
 */

public class Country extends Model {
    public static final String TABLE_NAME = "Country";

    private Long id;
    private static final String FIELD_ID = "id";

    @JsonProperty(value="NO_CIA")
    private String NO_CIA;
    public static final String FIELD_NO_CIA = "noCia";

    @JsonProperty
    private String MONEDA;
    public static final String FIELD_MONEDA = "moneda";

    @JsonProperty
    private String IDIOMA;
    public static final String FIELD_IDIOMA = "idioma";

    @JsonProperty(value="ABREVIATURA")
    private String ABREVIATURA;
    public static final String FIELD_ABREVIATURA = "ABREVIATURA";

    @JsonProperty
    private String LOYALTY;

    @JsonProperty
    private String PROMOTIONS;

    @JsonProperty
    private String ID_IDIOMA;

    @JsonProperty (value="FLAG")
    private String FLAG;
    public static final String FIELD_FLAG = "flag";

    @JsonProperty (value="PAIS")
    private String PAIS;
    public static final String FIELD_PAIS = "pais";

    @JsonProperty (value="NOMBRE")
    private String NOMBRE;
    public static final String FIELD_NOMBRE = "nombre";

    @JsonProperty
    private String DELIVERY;

    public Country() {

    }

    public Country(Long id, String NO_CIA, String MODENA, String IDIOMA, String ABREVIATURA, String NOMBRE,
                   String FLAG, String PAIS) {
        this.id = id;
        this.NO_CIA = NO_CIA;
        this.MONEDA = MODENA;
        this.IDIOMA = IDIOMA;
        this.ABREVIATURA = ABREVIATURA;
        this.NOMBRE = NOMBRE;
        this.FLAG = FLAG;
        this.PAIS = PAIS;
    }

    public static void createTable(SQLiteDatabase sqLiteDatabase) {
        String sqlCreate = SQL_CREATE_TABLE + TABLE_NAME + " (" +
                FIELD_ID + SQL_AUTOINCREMENT_ID +
                FIELD_NO_CIA + SQL_TYPE_TEXT + SQL_SEP +
                FIELD_MONEDA + SQL_TYPE_TEXT + SQL_SEP +
                FIELD_IDIOMA + SQL_TYPE_TEXT + SQL_SEP +
                FIELD_ABREVIATURA + SQL_TYPE_TEXT + SQL_SEP +
                FIELD_NOMBRE + SQL_TYPE_TEXT + SQL_SEP +
                FIELD_FLAG + SQL_TYPE_TEXT + SQL_SEP +
                FIELD_PAIS + SQL_TYPE_TEXT + ");";
        Log.i("SQL Create", sqlCreate);
        sqLiteDatabase.execSQL(sqlCreate);
    }

    public static void deleteTable(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_DELETE_IF_TABLE + TABLE_NAME);
    }

    public static List<Country> findAll(Context context) {
        return find(context, null, null);
    }

    public static List<Country> find(Context context,
                                     String whereClause,
                                     String[] whereArgs) {
        List<Country> result = new ArrayList<>();

        DatabaseManager databaseManager =
                new DatabaseManager(context);
        SQLiteDatabase sqLiteDatabase =
                databaseManager.getReadableDatabase();

        String[] requiredFields = {
                FIELD_ID,
                FIELD_ABREVIATURA,
                FIELD_IDIOMA,
                FIELD_MONEDA,
                FIELD_NO_CIA,
                FIELD_NOMBRE,
                FIELD_FLAG,
                FIELD_PAIS
        };

        Cursor cursor = sqLiteDatabase.query(
                TABLE_NAME,
                requiredFields,
                whereClause,
                whereArgs,
                null, null, null, null
        );

        if (cursor.moveToFirst()) {
            Country country;

            do {
                country = new Country(cursor.getLong(0),
                        cursor.getString(4),
                        cursor.getString(3),
                        cursor.getString(2),
                        cursor.getString(1),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7));

                result.add(country);
            } while (cursor.moveToNext());
        }

        sqLiteDatabase.close();
        databaseManager.close();

        return result;
    }

    @Override
    public boolean save(Context context) {
        boolean result = false;

        DatabaseManager databaseManager = new DatabaseManager(context);
        SQLiteDatabase sqLiteDatabase = databaseManager.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(FIELD_ABREVIATURA, ABREVIATURA);
        cv.put(FIELD_IDIOMA, IDIOMA);
        cv.put(FIELD_MONEDA, MONEDA);
        cv.put(FIELD_NO_CIA, NO_CIA);
        cv.put(FIELD_NOMBRE,NOMBRE);
        cv.put(FIELD_FLAG, FLAG);
        cv.put(FIELD_PAIS, PAIS);

        //update
        if (id != null) {
            String whereClause = FIELD_ID + "= ?";
            String[] whereArgs = {id.toString()};
            result = (sqLiteDatabase.update(TABLE_NAME, cv,
                    whereClause, whereArgs) > 0);
            //Create
        } else {
            id = sqLiteDatabase.insert(TABLE_NAME,
                    null, cv);
            result = (id > 0);
        }

        sqLiteDatabase.close();
        databaseManager.close();

        return result;
    }

    public boolean delete(Context context) {
        boolean result = false;

        if (id != null) {
            DatabaseManager databaseManager = new DatabaseManager(context);
            SQLiteDatabase sqLiteDatabase = databaseManager.getWritableDatabase();

            String whereClause = FIELD_ID + "= ?";
            String[] whereArgs = {id.toString()};

            result = (sqLiteDatabase.delete(TABLE_NAME,
                    whereClause, whereArgs) > 0);

            sqLiteDatabase.close();
            databaseManager.close();
        }

        return result;
    }

    public String getNO_CIA() {
        return NO_CIA;
    }

    public void setNO_CIA(String NO_CIA) {
        this.NO_CIA = NO_CIA;
    }

    public String getPAIS() {
        return PAIS;
    }

    public void setPAIS(String PAIS) {
        this.PAIS = PAIS;
    }

    public String getFLAG() {
        return FLAG;
    }

    public void setFLAG(String FLAG) {
        this.FLAG = FLAG;
    }

    public String getNOMBRE() {
        return NOMBRE;
    }

    public void setNOMBRE(String NOMBRE) {
        this.NOMBRE = NOMBRE;
    }

    public String getIDIOMA() {
        return IDIOMA;
    }

    public void setIDIOMA(String IDIOMA) {
        this.IDIOMA = IDIOMA;
    }
}
