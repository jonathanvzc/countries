package objects.ws;

/**
 * Created by Jonathan.Zepeda on 20/03/2018
 */

public class ServerResponse {
    private String message;
    private boolean sucessful;
    private String json;

    public ServerResponse() {
        this.sucessful = true;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSucessful() {
        return sucessful;
    }

    public void setSucessful(boolean sucessful) {
        this.sucessful = sucessful;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }
}
