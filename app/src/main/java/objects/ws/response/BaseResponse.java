package objects.ws.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Jonathan.Zepeda on 20/03/2018
 */

public class BaseResponse {

    @JsonProperty
    private String codigo;

    @JsonProperty(value = "mensaje")
    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSucessful() {
        return sucessful;
    }
    private boolean sucessful;

    public String getMessage() {
        return message;
    }


    public void setSucessful(boolean sucessful) {
        this.sucessful = sucessful;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
        this.sucessful = (codigo.equalsIgnoreCase("0000"));
    }
}
