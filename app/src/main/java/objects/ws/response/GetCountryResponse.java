package objects.ws.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import objects.beans.Country;

public class GetCountryResponse extends BaseResponse {
    @JsonProperty(value="resp")
    private List<Country> countryList;

    public List<Country> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<Country> countryList) {
        this.countryList = countryList;
    }
}
